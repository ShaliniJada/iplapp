package com.example.adminservice.entity;

import java.util.List;



import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "team_table")

@JsonIgnoreProperties(value={"handler","hibernateLazyInitializer","FieldHandler"})
public class Team {
	@Id
	 @GeneratedValue(strategy = GenerationType.AUTO)
	private Long teamId;
	private String teamName;
	private String state;
	private String ownerName;
	private String username;
	private String tempPassword;
	
	 @OneToMany(targetEntity = Player.class)
	    @JoinTable(name="team_players",
	    joinColumns = @JoinColumn(name="team_id"),
	    inverseJoinColumns = @JoinColumn(name="player_id"))
	    @JsonBackReference
	    private List<Player> players;
	

 /*@OneToMany(targetEntity = Player.class,cascade = CascadeType.ALL)
	@JoinColumn(name ="player_fk",referencedColumnName = "teamId")
	 @JoinTable(	name = "team_players",
     joinColumns = @JoinColumn(name = "team_Id"),
     inverseJoinColumns = @JoinColumn(name = "player_Id"))
	  private Player players;*/
	

}
