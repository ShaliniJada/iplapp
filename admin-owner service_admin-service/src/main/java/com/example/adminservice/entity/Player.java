package com.example.adminservice.entity;

import javax.persistence.CascadeType;


import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name= "play_list")



public class Player {

	@Id
	 @GeneratedValue(strategy = GenerationType.AUTO)
	 private Integer playerId;
    private String playerName;
    private Integer age;
    private Boolean isForegin;
    private Boolean isAvailable;
    private String specialty;
    private String imageUrl;
    private String nationality;
	
	/*@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "playerlist_fk")
   // @JsonBackReference
    private Team team;*/
	
    @ManyToOne(targetEntity = Team.class)
    @JoinColumn(name="team_id")
    private Team team;

	
	
}
