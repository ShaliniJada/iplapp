package com.example.adminservice.controller;




import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.adminservice.dto.TeamRequest;
import com.example.adminservice.entity.Team;
import com.example.adminservice.repository.PlayerRepo;
import com.example.adminservice.repository.TeamRepo;
import com.example.adminservice.services.TeamServiceImpl;
@RestController

@CrossOrigin("*")
public class TeamController {
	
	@Autowired
	 private TeamServiceImpl services;
	
	@PostMapping(value = "saveTeams")
	public String addNewTeam(@RequestBody Team teams) {
		System.out.println("At Controller Add");
		services.saveTeam(teams);
		return " teams Added Successfully";
	}
	
	@GetMapping(value = "findAllTeams")
	public List<Team> findAllTeam(){
		return services.getAllTeam();
	}
	
	
	@PutMapping(value = "updateTeams")
	public Team updateTeam(@RequestBody Team teams) {
		return services.updateTeam(teams);
	}
	
	
	@DeleteMapping(value = "deleteTeam")
	public String deleteTeam(@RequestParam Long teamId) {
		System.out.println("At deleted teams");
		services.deleteTeam(teamId);
		
		return "team " + teamId + " is deleted";
	}
	
/*	@Autowired
	private TeamRepo teams;
	
    @Autowired 
    private PlayerRepo players;
    
    @PostMapping("/save")
    public Team  saveTeam(@RequestBody TeamRequest request){
       return teams.save(request.getTeam());
    }

   @GetMapping("/findAll")
    public List<Team> findAllTeams(){
        return teams.findAll();
    }
    */
	
	
	
	
	
	
	
	
	

	
	 
	
	
}
