package com.example.adminservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.adminservice.entity.Player;

import com.example.adminservice.services.PlayerServiceImpl;

@RestController

@CrossOrigin("*")
public class PlayerController {

	
	
	@Autowired
	private PlayerServiceImpl playerservice;
	
	
	
	
	@PostMapping(value = "savePlayers")
	public String addNewPlayer(@RequestBody Player players) {
		System.out.println("At Controller Add");
		playerservice.savePlayer(players);
		return " players Added Successfully";
	}
	
	@GetMapping(value = "findAllPlayers")
	public List<Player> findAllTeam( Long teamId){
		return playerservice.getAllPlayer(teamId);
	}
	
	
	@PutMapping(value = "updatePlayer")
	public Player updatePlayer(@RequestBody Player player) {
		return playerservice.updatePlayer( player);
	}
	
	
	@DeleteMapping(value = "deletePlayer")
	public String deletePlayer(@RequestParam Long teamId) {
		System.out.println("At deleted players");
		playerservice.deletePlayer(teamId);
		
		return "team " + teamId + " is deleted";
		
		
		/*//owner's dashboard
				@GetMapping("/getPlayerByOwner/{ownerId}")
				public List<Player> getPlayerByOwner(@PathVariable int ownerId) {
					return pRepo.findByOwnerId(ownerId);
				}
				
				@PutMapping("/addToTeam/{ownerId}/{playerId}")
				public void addToTeam(@PathVariable int ownerId,@PathVariable int playerId) {
					
					Owner o = new Owner();
					o.setOwnerId(ownerId);
					Player player=pRepo.findById(playerId).orElse(null);
					player.setOwner(o);
					pRepo.save(player);
				}
				
				@PutMapping("/removePlayer/{playerId}")
				public void removePlayer(@PathVariable int playerId) {
					Player player=pRepo.findById(playerId).orElse(null);
					player.setOwner(null);
					pRepo.save(player);
				}*/
		
	
}
}
