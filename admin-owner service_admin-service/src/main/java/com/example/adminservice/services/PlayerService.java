package com.example.adminservice.services;

import java.util.List;

import com.example.adminservice.entity.Player;



public interface PlayerService {

	
	public void savePlayer(Player player);
	public List<Player> getAllPlayer( Long teamId);
	public Player updatePlayer(Player player);
	public void deletePlayer(Long teamId);
	

    
	
}
