package com.example.adminservice.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.adminservice.entity.Player;
import com.example.adminservice.entity.Team;
import com.example.adminservice.repository.PlayerRepo;

@Service
public class PlayerServiceImpl  implements PlayerService{
	
	
	@Autowired 
	 private PlayerRepo players;

	@Override
	public void savePlayer(Player player) {
		// TODO Auto-generated method stub
		// @SuppressWarnings("unused")
		//  Player updatePlayer= players.save(player);
		players.save(player);
	}

	@Override
	public List<Player> getAllPlayer( Long teamId) {
		// TODO Auto-generated method stub
		return (List<Player>) players.findAll();
	}

	@Override
	public Player updatePlayer(Player player) {
		// TODO Auto-generated method stub
		return players.save(player);
	}

	@Override
	public void deletePlayer(Long teamId) {
		// TODO Auto-generated method stub
		players.deleteById(teamId);
	}
	



}
