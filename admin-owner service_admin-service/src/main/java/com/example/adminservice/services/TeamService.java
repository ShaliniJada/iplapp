package com.example.adminservice.services;

import java.util.List;

import com.example.adminservice.dto.TeamRequest;
import com.example.adminservice.entity.Team;

public interface TeamService {
	
	public void saveTeam(Team team);
	public List<Team> getAllTeam();
	public Team updateTeam(Team team);
	public void deleteTeam(Long teamId);
	
	//public Team saveTeam(TeamRequest request);
	// public Team getTeamById(Integer id);
	
	/* public Team addTeam(Team team);
	    public void deleteTeam(Long teamId);
	    public List<Team> viewTeams();

	    public Team getTeamById(Integer id);*/


}
