package com.example.adminservice.dto;

import com.example.adminservice.entity.Team;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TeamRequest {
	
	
	
	private Team team;
	

}
