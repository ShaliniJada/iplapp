package com.example.adminservice.repository;


import org.springframework.data.jpa.repository.JpaRepository;


import org.springframework.stereotype.Repository;

import com.example.adminservice.entity.Player;
@Repository
public interface PlayerRepo extends JpaRepository<Player,Long> {




}
